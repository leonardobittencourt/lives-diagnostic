import Knex from 'knex';
import LogHelper from '../../helpers/log.helper';
import { ZoomHooksConnection } from '../../database/connection';
import { IZoomHooksRecordings } from './interfaces/zoom-hooks-recordings.interface';

export default class ZoomHooksRecordingsRepository {
  private readonly tableName = 'zoom_hook_recordings';
  private readonly logHelper;
  private readonly connection: Knex;

  constructor({ logHelper = new LogHelper(), connection = ZoomHooksConnection } = {}) {
    this.logHelper = logHelper;
    this.connection = connection;
  }

  async findOneByLiveExternalId(liveExternalId: string): Promise<IZoomHooksRecordings[]> {
    return await this.connection
      .table<IZoomHooksRecordings>(this.tableName)
      .select('*')
      .where('meeting_id', '=', liveExternalId)
      .orderBy('created_at', 'desc');
  }
}
