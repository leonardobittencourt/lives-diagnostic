if (process.env.NODE_ENV !== 'production') require('dotenv').config({ path: __dirname + '/../.env' });
import LogHelper from './helpers/log.helper';
import { AppService } from './app/app.service';

const logHelper = new LogHelper();
const appService = new AppService();

async function main() {
  logHelper.log('Start');

  await appService.run();

  logHelper.log('Finished');
}

main();
