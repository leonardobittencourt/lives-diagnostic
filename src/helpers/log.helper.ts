import * as fs from 'fs';
import FileHelper from './file.helper';

class LogHelper {
  private fileHelper;

  constructor({ fileHelper = new FileHelper() } = {}) {
    this.fileHelper = fileHelper;
  }

  log(message: string) {
    process.env.DEBUG ? console.log(message) : null;
  }

  saveLog(data: any) {
    this.fileHelper.saveLog(data, `${Date.now()}.json`, '../logs');
  }
}

export default LogHelper;
