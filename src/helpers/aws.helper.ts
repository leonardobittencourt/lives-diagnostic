import * as AWS from 'aws-sdk';
import { S3 } from 'aws-sdk';
import LogHelper from './log.helper';

class AwsHelper {
  private S3Instance: S3;
  private logHelper;

  constructor({ logHelper = new LogHelper() } = {}) {
    this.logHelper = logHelper;

    AWS.config.update({
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    });

    this.S3Instance = new AWS.S3({ apiVersion: '2006-03-01', region: 'sa-east-1', signatureVersion: 'v4' });
  }

  async getSignedUrlToGetObject(filePath: string) {
    const url = await this.S3Instance.getSignedUrlPromise('getObject', {
      Bucket: process.env.AWS_BUCKET_NAME,
      Key: filePath,
      Expires: parseInt(process.env.AWS_SIGNED_URL_EXPIRES || '3600'),
    });

    return { url };
  }

  async getObjectFromList(filePath: string) {
    const s3Response = await this.S3Instance.listObjects({
      Bucket: process.env.AWS_BUCKET_NAME || '',
      Prefix: filePath,
    }).promise();

    return s3Response.Contents ? s3Response.Contents[0] : null;
  }

  async isFileExists(filePath: string): Promise<boolean> {
    this.logHelper.log(`[AWS][isFileExists] ${filePath}`);

    const s3Response = await this.S3Instance.listObjects({
      Bucket: process.env.AWS_BUCKET_NAME || '',
      Prefix: filePath,
    }).promise();

    let fileSize = 0;
    if (s3Response && s3Response?.Contents) {
      fileSize = s3Response.Contents[0]?.Size || 0;

      this.logHelper.log(JSON.stringify(s3Response.Contents[0], null, 2));
    }

    return fileSize > 0;
  }

  async getSignedUrlToPutObject(fileName: string, folderName: string) {
    this.logHelper.log('[AWS-GET-SIGNED-URL] BEGIN');

    const filePath = this.generateFilePath(fileName, folderName);

    const url = await this.S3Instance.getSignedUrlPromise('putObject', {
      Bucket: process.env.AWS_BUCKET_NAME,
      Key: filePath,
      Expires: parseInt(process.env.AWS_SIGNED_URL_EXPIRES || '3600'),
    });

    this.logHelper.log('[AWS-GET-SIGNED-URL] END');

    return { filePath, url };
  }

  private generateFilePath(fileName: string, folderName: string): string {
    let filePath = '';

    if (folderName) filePath += folderName + '/';

    filePath += fileName;

    return filePath;
  }

  async changeFromGlacierToStandard(Key: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.S3Instance.restoreObject(
        {
          Bucket: process.env.AWS_BUCKET_NAME || '',
          Key,
          RestoreRequest: {
            Days: 20,
            GlacierJobParameters: {
              Tier: 'Standard',
            },
          },
        },
        (err) => {
          if (err) {
            this.logHelper.log(`Error ${Key} - ${err.message}`);
            reject(`Error ${Key} - ${err.message}`);
            return;
          }

          this.logHelper.log('Success');
          resolve(true);
        },
      );
    });
  }
}

export default AwsHelper;
