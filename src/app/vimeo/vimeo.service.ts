import HttpHelper from '../../helpers/http.helper';
import LogHelper from '../../helpers/log.helper';
import { IVideo } from './interfaces/vimeo.interface';

class VimeoService {
  private readonly BASE_URL = process.env.VIMEO_BASE_URL;
  private readonly TOKEN = process.env.VIMEO_TOKEN;
  private readonly logHelper;
  private readonly httpHelper;

  constructor({
    logHelper = new LogHelper(),
    httpHelper = new HttpHelper({ checkApiRateLimit: true, limitRequestsPerSecond: 12, timeToRest: 1000 }),
  } = {}) {
    this.logHelper = logHelper;
    this.httpHelper = httpHelper;
  }

  async getVideoById(uri: string): Promise<IVideo | undefined> {
    const url = `${this.BASE_URL}${uri}`;
    const config = { headers: this.getHeaders() };

    this.logHelper.log(`[getVideoById][${uri}] BEGIN`);
    try {
      const response = await this.httpHelper.get<IVideo>(url, config);
      if (response && response.status === 200 && response.data) {
        return response.data;
      }
    } catch (e) {
      this.logHelper.log(`[getVideoById] ERROR ${e.message}`);
    } finally {
      this.logHelper.log(`[getVideoById][${uri}] END`);
    }
  }

  async deleteVideoById(uri: string) {
    const url = `${this.BASE_URL}${uri}`;
    const config = { headers: this.getHeaders() };

    this.logHelper.log(`[deleteVideoById][${url}] BEGIN`);
    try {
      const response = await this.httpHelper.delete(url, config);
      if (response && response.status !== 204) {
        throw new Error('An unexpected error occured when try delete a video');
      }
    } catch (e) {
      this.logHelper.log(`[deleteVideoById] ERROR ${e.message}`);
    } finally {
      this.logHelper.log(`[deleteVideoById][${url}] END`);
    }
  }

  private getHeaders() {
    return {
      authorization: `Bearer ${this.TOKEN}`,
    };
  }
}

export default VimeoService;
