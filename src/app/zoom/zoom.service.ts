import FileHelper from '../../helpers/file.helper';
import HttpHelper from '../../helpers/http.helper';
import LogHelper from '../../helpers/log.helper';
import SqlHelper from '../../helpers/sql.helper';
import ZoomHooksRecordingsService from '../zoom-hooks-recordings/zoom-hooks-recordings.service';
import { IMeetingParticipants } from './interfaces/participants.interface';
import SqlGenerateInterface from './interfaces/sql-generate.interface';
import ZoomRecordingsInterface from './interfaces/zoom-recordings.interface';
import { IZoomRecording } from './interfaces/zoom.interface';

export default class ZoomService {
  private readonly BASE_URL = process.env.ZOOM_BASE_URL;
  private readonly TOKEN = process.env.ZOOM_TOKEN;
  private readonly httpHelper;
  private readonly logHelper;
  private readonly sqlHelper;
  private readonly fileHelper;
  private readonly zoomHooksRecordingsService;

  constructor({
    logHelper = new LogHelper(),
    httpHelper = new HttpHelper({ checkApiRateLimit: true, limitRequestsPerSecond: 12, timeToRest: 1000 }),
    sqlHelper = new SqlHelper(),
    fileHelper = new FileHelper(),
    zoomHooksRecordingsService = new ZoomHooksRecordingsService(),
  } = {}) {
    this.httpHelper = httpHelper;
    this.logHelper = logHelper;
    this.sqlHelper = sqlHelper;
    this.fileHelper = fileHelper;
    this.zoomHooksRecordingsService = zoomHooksRecordingsService;
  }

  async getRecordingByLiveExternalId(liveExternalId: string) {
    const url = `${this.BASE_URL}/meetings/${liveExternalId}/recordings`;
    const config = { headers: this.getHeaders() };

    try {
      const response = await this.httpHelper.get<IZoomRecording>(url, config);
      if (response && response.status === 200 && response.data) {
        return response.data;
      }
    } catch (e) {
      this.logHelper.log(`[getRecordingByLiveExternalId] ERROR - ${e.message}`);
    }
  }

  async getMeetingParticipantsReport(liveExternalId: string) {
    const url = `${this.BASE_URL}/report/meetings/${liveExternalId}/participants`;
    const config = { params: { page_size: 300 }, headers: this.getHeaders() };

    try {
      const response = await this.httpHelper.get<IMeetingParticipants>(url, config);
      if (response && response.status === 200 && response.data) {
        return response.data;
      }
    } catch (e) {
      this.logHelper.log(`[getMeetingParticipantsReport] ERROR - ${e.message}`);
    }
  }

  // get information of zoom hooks recorddings
  async getInfosZoomHookRecordings(liveExternalId: string): Promise<ZoomRecordingsInterface> {
    try {
      const url = `${this.BASE_URL}/meetings/${liveExternalId}/recordings`;
      const options = {
        headers: { Authorization: `Bearer ${process.env.ZOOM_TOKEN}` },
      };

      const { data, status } = await this.httpHelper.get<ZoomRecordingsInterface>(url, options);

      if (status !== 200) {
        const messagError = `[SQL Generator] recordings founded [meetingId: ${liveExternalId}]`;
        this.logHelper.log(messagError);
        throw new Error(messagError);
      }

      return data;
    } catch (e) {
      throw new Error('Has a problem: ' + e.message);
    }
  }

  // main to processing data to generate querys sql
  async generateSqlZoomHookRecordings(lives: SqlGenerateInterface[]) {
    this.logHelper.log('[SQL Generator] Begin process of the generation.');

    for (const live of lives) {
      let recordings;

      try {
        this.logHelper.log('[SQL Generator] Processing the recording: ' + live.liveExternalId);

        // get all information with live external id
        recordings = await this.getInfosZoomHookRecordings(live.liveExternalId);
      } catch (e) {
        this.logHelper.log(e.message);
      } finally {
        this.logHelper.log('[SQL Generator] Finish process on metting id: ' + live.liveExternalId);
      }

      if (recordings) {
        this.logHelper.saveLog(JSON.stringify(recordings, null, 2));

        await this.zoomHooksRecordingsService.save(recordings);

        const query = this.sqlHelper.createQueryZoomHooksRecordings(recordings, live.ssoId);
        this.fileHelper.saveLog(query, `${live.liveExternalId}.sql`, '../sql');
      }
    }

    console.log('[MAIN] end');
  }

  private getHeaders() {
    return {
      authorization: `Bearer ${this.TOKEN}`,
    };
  }
}
