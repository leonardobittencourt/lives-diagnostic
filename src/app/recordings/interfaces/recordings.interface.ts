export interface IRecordings {
  id: string;
  platform: string;
  live_external_id: string;
  download_url: string;
  play_url: string;
  uploaded_to: string;
  file_type: string;
  file_size: string;
  hook_id: string;
  backup_status: string;
  show_recording: string;
  can_download: string;
  created_at: string;
  updated_at: string;
  deleted_at: string;
}
