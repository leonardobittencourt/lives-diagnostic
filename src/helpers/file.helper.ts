import fs from 'fs';
import path from 'path';
import { ILives } from '../app/lives/interfaces/lives.interface';

export default class FileHelper {
  saveLog = (sql: any, fileName: string, folderName: string) => {
    if (!fs.existsSync(path.join(__dirname, folderName))) {
      fs.mkdir(path.join(__dirname, folderName), (err) => {
        if (err) {
          return console.error(err);
        }

        this.writeFile(sql, fileName, folderName);
        console.log('Directory created successfully!');
      });
    } else {
      this.writeFile(sql, fileName, folderName);
    }
  };

  writeFile = (sql: any, fileName: string, folderName: string) => {
    fs.writeFile(path.join(__dirname, `${folderName}/${fileName}`), sql, (err: any) => {
      if (err) throw new Error(err);

      console.log(`[${fileName}] Log save successfully`);
    });
  };

  saveResultsToFile(live: ILives, results: any[], diferentDir: string = 'logs'): void {
    const dir = `${__dirname}/../results/${diferentDir}`;
    if (!fs.existsSync(dir)) fs.mkdirSync(dir, { recursive: true });

    fs.writeFileSync(`${dir}/${live.id}.json`, JSON.stringify(results, null, 2));
  }
}
