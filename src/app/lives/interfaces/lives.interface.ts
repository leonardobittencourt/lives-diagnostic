export interface ILives {
  id: string;
  title: string;
  description: string;
  sso_id: string;
  sso_name: string;
  live_date: string;
  start_hour: string;
  end_hour: string;
  attendance_limit: number;
  is_recurring: boolean;
  is_active: boolean;
  platform: string;
  timezone: string;
  live_password: string;
  join_url: string;
  start_url: string;
  live_external_id: string;
  created_at: string;
  updated_at: string;
  deleted_at: string;
  privacy: string;
  start_at: string;
  end_at: string;
  show_recording: boolean;
  can_download_recordings: boolean;
}
