import ZoomRecordingsFileInterface from './zoom-recordings-file.interface';

export default interface ZoomRecordingsInterface {
  uuid: string;
  id: number;
  accountId: string;
  hostId: string;
  topic: string;
  type: number;
  start_time: string;
  timezone: string;
  host_email: string;
  duration: number;
  total_size: number;
  recording_count: number;
  share_url: number;
  recording_files: ZoomRecordingsFileInterface[];
}
