export default interface ZoomRecordingsFileInterface {
  id: string;
  meeting_id: string;
  recording_start: Date;
  recording_end: Date;
  file_type: string;
  file_extension: string;
  file_size: number;
  play_url: string;
  download_url: string;
  status: string;
  recording_type: string;
}
