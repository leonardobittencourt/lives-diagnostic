export interface Paging {
  next: string;
  previous?: any;
  first: string;
  last: string;
}

export interface Buttons {
  like: boolean;
  watchlater: boolean;
  share: boolean;
  embed: boolean;
  hd: boolean;
  fullscreen: boolean;
  scaling: boolean;
}

export interface Custom {
  active: boolean;
  url?: any;
  link?: any;
  sticky: boolean;
}

export interface Logos {
  vimeo: boolean;
  custom: Custom;
}

export interface Title {
  name: string;
  owner: string;
  portrait: string;
}

export interface Live {
  streaming: boolean;
  archived: boolean;
}

export interface StaffPick {
  normal: boolean;
  best_of_the_month: boolean;
  best_of_the_year: boolean;
  premiere: boolean;
}

export interface Badges {
  hdr: boolean;
  live: Live;
  staff_pick: StaffPick;
  vod: boolean;
  weekend_challenge: boolean;
}

export interface Embed {
  buttons: Buttons;
  logos: Logos;
  title: Title;
  playbar: boolean;
  volume: boolean;
  speed: boolean;
  color: string;
  uri: string;
  html: string;
  badges: Badges;
}

export interface Privacy {
  view: string;
  embed: string;
  download: boolean;
  add: boolean;
  comments: string;
}

export interface Size {
  width: number;
  height: number;
  link: string;
  link_with_play_button: string;
}

export interface Pictures {
  uri: string;
  active: boolean;
  type: string;
  sizes: Size[];
  resource_key: string;
  default_picture: boolean;
}

export interface Stats {
  plays: number;
}

export interface Comments {
  uri: string;
  options: string[];
  total: number;
}

export interface Likes {
  uri: string;
  options: string[];
  total: number;
}

export interface Pictures2 {
  uri: string;
  options: string[];
  total: number;
}

export interface Texttracks {
  uri: string;
  options: string[];
  total: number;
}

export interface Related {
  uri: string;
  options: string[];
}

export interface Albums {
  uri: string;
  options: string[];
  total: number;
}

export interface AvailableAlbums {
  uri: string;
  options: string[];
  total: number;
}

export interface Versions {
  uri: string;
  options: string[];
  total: number;
  current_uri: string;
  resource_key: string;
}

export interface Connections {
  comments: Comments;
  credits?: any;
  likes: Likes;
  pictures: Pictures2;
  texttracks: Texttracks;
  related: Related;
  recommendations?: any;
  albums: Albums;
  available_albums: AvailableAlbums;
  versions: Versions;
}

export interface Watchlater {
  uri: string;
  options: string[];
  added: boolean;
  added_time?: any;
}

export interface Report {
  uri: string;
  options: string[];
  reason: string[];
}

export interface ViewTeamMembers {
  uri: string;
  options: string[];
}

export interface Edit {
  uri: string;
  options: string[];
}

export interface Interactions {
  watchlater: Watchlater;
  report: Report;
  view_team_members: ViewTeamMembers;
  edit: Edit;
}

export interface Metadata {
  connections: Connections;
  interactions: Interactions;
}

export interface Size2 {
  width: number;
  height: number;
  link: string;
}

export interface Pictures3 {
  uri: string;
  active: boolean;
  type: string;
  sizes: Size2[];
  resource_key: string;
  default_picture: boolean;
}

export interface Albums2 {
  uri: string;
  options: string[];
  total: number;
}

export interface Appearances {
  uri: string;
  options: string[];
  total: number;
}

export interface Categories {
  uri: string;
  options: string[];
  total: number;
}

export interface Channels {
  uri: string;
  options: string[];
  total: number;
}

export interface Feed {
  uri: string;
  options: string[];
}

export interface Followers {
  uri: string;
  options: string[];
  total: number;
}

export interface Following {
  uri: string;
  options: string[];
  total: number;
}

export interface Groups {
  uri: string;
  options: string[];
  total: number;
}

export interface Likes2 {
  uri: string;
  options: string[];
  total: number;
}

export interface Membership {
  uri: string;
  options: string[];
}

export interface ModeratedChannels {
  uri: string;
  options: string[];
  total: number;
}

export interface Portfolios {
  uri: string;
  options: string[];
  total: number;
}

export interface Videos {
  uri: string;
  options: string[];
  total: number;
}

export interface Watchlater2 {
  uri: string;
  options: string[];
  total: number;
}

export interface Shared {
  uri: string;
  options: string[];
  total: number;
}

export interface Pictures4 {
  uri: string;
  options: string[];
  total: number;
}

export interface WatchedVideos {
  uri: string;
  options: string[];
  total: number;
}

export interface FoldersRoot {
  uri: string;
  options: string[];
}

export interface Folders {
  uri: string;
  options: string[];
  total: number;
}

export interface Teams {
  uri: string;
  options: string[];
  total: number;
}

export interface Block {
  uri: string;
  options: string[];
  total: number;
}

export interface Connections2 {
  albums: Albums2;
  appearances: Appearances;
  categories: Categories;
  channels: Channels;
  feed: Feed;
  followers: Followers;
  following: Following;
  groups: Groups;
  likes: Likes2;
  membership: Membership;
  moderated_channels: ModeratedChannels;
  portfolios: Portfolios;
  videos: Videos;
  watchlater: Watchlater2;
  shared: Shared;
  pictures: Pictures4;
  watched_videos: WatchedVideos;
  folders_root: FoldersRoot;
  folders: Folders;
  teams: Teams;
  block: Block;
}

export interface Metadata2 {
  connections: Connections2;
}

export interface LocationDetails {
  formatted_address: string;
  latitude?: any;
  longitude?: any;
  city?: any;
  state?: any;
  neighborhood?: any;
  sub_locality?: any;
  state_iso_code?: any;
  country?: any;
  country_iso_code?: any;
}

export interface Privacy2 {
  view: string;
  comments: string;
  embed: string;
  download: boolean;
  add: boolean;
}

export interface Videos2 {
  privacy: Privacy2;
}

export interface Preferences {
  videos: Videos2;
}

export interface User {
  uri: string;
  name: string;
  link: string;
  location: string;
  gender: string;
  bio?: any;
  short_bio?: any;
  created_time: Date;
  pictures: Pictures3;
  websites: any[];
  metadata: Metadata2;
  location_details: LocationDetails;
  skills: any[];
  available_for_hire: boolean;
  can_work_remotely: boolean;
  preferences: Preferences;
  content_filter: string[];
  resource_key: string;
  account: string;
}

export interface ReviewPage {
  active: boolean;
  link: string;
}

export interface File {
  quality: string;
  type: string;
  width: number;
  height: number;
  link: string;
  created_time: Date;
  fps: number;
  size: number;
  md5: string;
  public_name: string;
  size_short: string;
}

export interface IVimeoDownloadFile {
  quality: string;
  type: string;
  width: number;
  height: number;
  expires: Date;
  link: string;
  created_time: Date;
  fps: number;
  size: number;
  md5: string;
  public_name: string;
  size_short: string;
}

export interface App {
  name: string;
  uri: string;
}

export interface Upload {
  status: string;
  upload_link: string;
  form?: any;
  complete_uri?: any;
  approach: string;
  size?: number;
  redirect_url?: any;
  link?: any;
}

export interface Transcode {
  status: string;
}

export interface IVideo {
  uri: string;
  name: string;
  description: string;
  type: string;
  link: string;
  duration: number;
  width: number;
  language?: any;
  height: number;
  embed: Embed;
  created_time: string;
  modified_time: Date;
  release_time: Date;
  content_rating: string[];
  license?: any;
  privacy: Privacy;
  pictures: Pictures;
  tags: any[];
  stats: Stats;
  categories: any[];
  metadata: Metadata;
  user: User;
  parent_folder?: any;
  last_user_action_event_date: Date;
  review_page: ReviewPage;
  files: File[];
  download: IVimeoDownloadFile[];
  app: App;
  status: string;
  resource_key: string;
  upload: Upload;
  transcode: Transcode;
  is_playable: boolean;
}

export interface IHttpData<T> {
  total: number;
  page: number;
  per_page: number;
  paging: Paging;
  data: T;
}
