# Live Recording Diagnostic

Esse repositório tem por objetivo realizar consultas em todos serviços (Zoom, Vimeo, AWS S3) e os Banco de Dados do AAV, afim de, buscar informações relacionadas as gravações de uma live

## Configurações

Copie o arquivo .env.example, crie um novo arquivo a partir dele chamado .env e preencha as informações necessárias.

## TODO

Algumas atividades de software que podem ser otimizadas caso ocorram algum problema referente ao mesmo.

### Problema #1

problema de usuários que participaram do meeting, mas não esta no relatorios

- Esse problema acontece por causa do relatorio gerado em cache, os relatorios das meetings são gerados apenas uma vez e adicionado um json no Amazon AWS S3, e depois de gerado a API pega o json da S3 e repassa para o cliente.

### Solução #1

Para poder resolver isso, basicamente quando identificamos que na tabela `zoom_hooks_participants` no banco `db_aav_zoom_hooks_*`, podemos buscar o dado participante se existe na coluna `event_name` como os valores de `meeting.participant_joined` e `meeting.participant_left`, caso tenha então ele existe no banco e participou da meeting, então o arquivo cache da S3 possui algum problema, sendo assim o que deve ser feito é a deleção do arquivo de cache.
