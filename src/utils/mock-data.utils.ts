export const fileSizeMock = (fileSize: number): number => {
  return fileSize > 2147483647 ? 2147483647 : fileSize;
};
