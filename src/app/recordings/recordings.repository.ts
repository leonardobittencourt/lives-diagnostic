import Knex from 'knex';
import LogHelper from '../../helpers/log.helper';
import { RecordingsConnection } from '../../database/connection';
import { IRecordings } from './interfaces/recordings.interface';

export default class RecordingsRepository {
  private readonly tableName = 'recordings';
  private readonly logHelper;
  private readonly connection: Knex;

  constructor({ logHelper = new LogHelper(), connection = RecordingsConnection } = {}) {
    this.logHelper = logHelper;
    this.connection = connection;
  }

  async findOneByLiveExternalId(liveExternalId: string): Promise<IRecordings[]> {
    return await this.connection
      .table<IRecordings>(this.tableName)
      .select('*')
      .where('live_external_id', '=', liveExternalId)
      .andWhere('file_type', '=', 'MP4')
      .orderBy('created_at', 'asc');
  }
}
