import LogHelper from '../../helpers/log.helper';
import { LiveConnection } from '../../database/connection';
import Knex from 'knex';
import { ILives } from './interfaces/lives.interface';

export default class LivesRepository {
  private readonly tableName = 'lives';
  private readonly logHelper;
  private readonly connection: Knex;

  constructor({ logHelper = new LogHelper(), connection = LiveConnection } = {}) {
    this.logHelper = logHelper;
    this.connection = connection;
  }

  async findOneById(id: string): Promise<ILives> {
    const [live] = await this.connection
      .table<ILives>(this.tableName)
      .select('*')
      .where('id', '=', id)
      .orderBy('start_at', 'desc')
      .limit(1);

    return live;
  }
}
