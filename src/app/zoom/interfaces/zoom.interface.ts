export interface IZoomRecordingFile {
  id: string;
  meeting_id: string;
  recording_start: Date;
  recording_end: Date;
  file_type: string;
  file_size: number;
  play_url: string;
  download_url: string;
  status: string;
  recording_type: string;
}

export interface IZoomRecording {
  uuid: string;
  id: number;
  account_id: string;
  host_id: string;
  topic: string;
  type: number;
  start_time: Date;
  timezone: string;
  duration: number;
  total_size: number;
  recording_count: number;
  share_url: string;
  recording_files: IZoomRecordingFile[];
}
