import { EventNameEnum } from '../app/zoom-hooks-participants/enum/event-name.enum';
import ZoomRecordingsInterface from '../app/zoom/interfaces/zoom-recordings.interface';
import * as dateUtils from '../utils/date.utils';
import * as mockUtils from '../utils/mock-data.utils';

export default class SqlHelper {
  createQueryZoomHooksRecordings(data: ZoomRecordingsInterface, ssoId: string): string {
    if (!data) throw new Error('An error ocurred: data is empty');

    let sql = ``;

    (data.recording_files || []).forEach((recording) => {
      const object = {
        event_name: 'recording.completed',
        meeting_id: `${data.id}`,
        meeting_type: data.type,
        meeting_start_time: `${data.start_time}`,
        meeting_timezone: `${data.timezone}`,
        meeting_duration: data.duration,
        meeting_host_email: `${ssoId}@conexia.com.br`,
        recording_id: `${recording.id}`,
        recording_meeting_id: `${recording.meeting_id}`,
        recording_start: `${dateUtils.formatDate(recording.recording_start)}`,
        recording_end: `${dateUtils.formatDate(recording.recording_end)}`,
        recording_status: `${recording.status}`,
        recording_type: `${recording.recording_type}`,
        file_type: `${recording.file_type}`,
        file_size: mockUtils.fileSizeMock(recording.file_size),
        download_url: `${recording.download_url}`,
        status_transfer: 'WAITING',
      };

      const columns = Object.keys(object);
      const values = Object.values(object).map((i) => (typeof i === 'string' ? `'${i}'` : i));

      sql += `INSERT INTO zoom_hook_recordings (${columns.join(
        ', ',
      )}, start_transfer, end_transfer, created_at) VALUES (${values.join(', ')}, null, null, NOW());`;
      sql += `
    `;
    });

    return sql;
  }

  createQueryZoomHooksParticipants(data: any): string {
    if (!data) throw new Error('An error ocurred: data is empty');

    let sql = ``;

    (data || []).forEach((item: any) => {
      const object = {
        event_name: EventNameEnum.LEFT,
        meeting_id: `${item.getParticipantsDontHasRegisterClassLeft.meeting_id}`,
        meeting_type: item.getParticipantsDontHasRegisterClassLeft.meeting_type,
        meeting_start_time: `${item.getParticipantsDontHasRegisterClassLeft.meeting_start_time}`,
        meeting_timezone: `${item.getParticipantsDontHasRegisterClassLeft.meeting_timezone}`,
        meeting_duration: item.zoomParticipants.duration,
        account_id_meeting_host: item.getParticipantsDontHasRegisterClassLeft.account_id_meeting_host,
        meeting_uuid: item.getParticipantsDontHasRegisterClassLeft.meeting_uuid,
        user_host_id: item.getParticipantsDontHasRegisterClassLeft.user_host_id,
        meeting_topic: item.getParticipantsDontHasRegisterClassLeft.meeting_topic,
        participant_user_id: item.getParticipantsDontHasRegisterClassLeft.participant_user_id,
        participant_user_name: item.getParticipantsDontHasRegisterClassLeft.participant_user_name,
        participant_id: '',
        participant_time: item.zoomParticipants.leave_time,
      };

      const columns = Object.keys(object);
      const values = Object.values(object).map((i) => (typeof i === 'string' ? `'${i}'` : i));

      sql += `INSERT INTO zoom_hook_participants (${columns.join(
        ', ',
      )}, start_transfer, end_transfer, created_at) VALUES (${values.join(', ')}, null, null, NOW());`;
      sql += `
    `;
    });

    return sql;
  }
}
