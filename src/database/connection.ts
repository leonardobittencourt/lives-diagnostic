import knex from 'knex';

const environment = process.env.NODE_ENV || 'development';

const livesConfig = require(__dirname + '/knexfile-lives.js')[environment];
const recordingsConfig = require(__dirname + '/knexfile-recordings.js')[environment];
const zoomHooksRecordingsConfig = require(__dirname + '/knexfile-zoom-hooks-recordings.js')[environment];

export const LiveConnection = knex(livesConfig);
export const RecordingsConnection = knex(recordingsConfig);
export const ZoomHooksConnection = knex(zoomHooksRecordingsConfig);
