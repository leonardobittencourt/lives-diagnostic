import LogHelper from '../../helpers/log.helper';
import { LiveConnection } from '../../database/connection';
import Knex from 'knex';
import { ILiveParticipants } from './interfaces/live-participants.interface';

export default class LiveParticipantsRepository {
  private readonly tableName = 'live_participants';
  private readonly logHelper;
  private readonly connection: Knex;

  constructor({ logHelper = new LogHelper(), connection = LiveConnection } = {}) {
    this.logHelper = logHelper;
    this.connection = connection;
  }

  async findByLiveId(liveId: string): Promise<ILiveParticipants[]> {
    return await this.connection
      .table<ILiveParticipants>(this.tableName)
      .select('*')
      .where('live_id', '=', liveId)
      .orderBy('created_at', 'desc');
  }
}
