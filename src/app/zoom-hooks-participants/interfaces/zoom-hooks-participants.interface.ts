export interface IZoomHooksParticipants {
  id: string;
  event_name: string;
  meeting_id: string;
  meeting_type: string;
  meeting_start_time: string;
  meeting_timezone: string;
  meeting_duration: string;
  account_id_meeting_host: string
  meeting_uuid: string
  user_host_id: string
  meeting_topic: string
  participant_user_id: string
  participant_user_name: string
  participant_id: string
  participant_time: string
  start_transfer: string
  end_transfer: string
  created_at: string
  updated_at: string
  deleted_at: string
}
