const Table = require('terminal-table');
export default class StatisticsService {
  private table;

  constructor({} = {}) {
    this.table = new Table({
      borderStyle: 3,
      horizontalLine: true,
      // width: [3, '30%', '20%', '50%'],
      rightPadding: 0,
      leftPadding: 1,
    });

    this.table.attrRange(
      { row: [0, 1] },
      {
        align: 'center',
        color: 'blue',
        bg: 'black',
      },
    );

    this.table.attrRange(
      { column: [0, 1] },
      {
        color: 'green',
      },
    );

    this.table.attrRange(
      {
        row: [1],
        column: [1],
      },
      {
        leftPadding: 5,
        rightPadding: 5,
      },
    );
  }

  Show = () => {
    console.log('' + this.table);
  };

  AddRow = (rowIndex: number, data: string[]) => {
    this.table.push(data);
    data.forEach((value, index) => {
      if (value == 'true') {
        this.table.attr(rowIndex + 1, index, {
          align: 'center',
          color: 'white',
          bg: 'green',
        });
      } else if (value == 'false') {
        this.table.attr(rowIndex + 1, index, {
          align: 'center',
          color: 'white',
          bg: 'red',
        });
      }
    });
  };

  AddHeader = (data: string[]) => {
    this.table.push(data);
  };
}
