export enum EventNameEnum {
  JOINED = 'meeting.participant_joined',
  LEFT = 'meeting.participant_left',
}
