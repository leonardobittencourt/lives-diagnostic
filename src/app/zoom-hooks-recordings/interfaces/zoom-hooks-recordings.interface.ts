export interface IZoomHooksRecordings {
  id: string;
  event_name: string;
  meeting_id: string;
  meeting_type: string;
  meeting_start_time: string;
  meeting_timezone: string;
  meeting_duration: string;
  meeting_host_email: string;
  recording_id: string;
  recording_meeting_id: string;
  recording_start: string;
  recording_end: string;
  recording_status: string;
  recording_type: string;
  file_type: string;
  file_size: string;
  download_url: string;
  status_transfer: string;
  start_transfer: string;
  end_transfer: string;
  created_at: string;
  live_id: string;
}
