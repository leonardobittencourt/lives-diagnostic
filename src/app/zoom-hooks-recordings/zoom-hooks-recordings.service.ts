import LogHelper from '../../helpers/log.helper';
import HttpHelper from '../../helpers/http.helper';
import ZoomRecordingsInterface from '../zoom/interfaces/zoom-recordings.interface';
import { fileSizeMock } from '../../utils/mock-data.utils';

export default class ZoomHooksRecordingsService {
  private readonly BASE_URL = process.env.ZOOM_HOOK_BASE_URL;
  private logHelper;
  private httpHelper;

  constructor({
    logHelper = new LogHelper(),
    httpHelper = new HttpHelper({ checkApiRateLimit: true, limitRequestsPerSecond: 12, timeToRest: 1000 }),
  } = {}) {
    this.logHelper = logHelper;
    this.httpHelper = httpHelper;
  }

  async save(data: ZoomRecordingsInterface) {
    const url = `${this.BASE_URL}/api/v1/recordings/completed`;

    const payload = {
      event: 'recording.completed',
      payload: {
        account_id: data.accountId,
        object: {
          id: data.id,
          type: data.type,
          start_time: data.start_time,
          timezone: data.timezone,
          duration: data.duration,
          host_email: data.host_email,
          recording_files: data.recording_files.map((recording) => ({
            ...recording,
            file_size: fileSizeMock(recording.file_size),
          })),
        },
      },
    };

    try {
      console.log(url, payload);
      const response = await this.httpHelper.post(url, payload);
      if (response.status === 201) {
        this.logHelper.log('[ZoomHooksRecordings] New hook save on API');
      }
    } catch (e) {
      console.log(e.response.data);
      this.logHelper.log('[ZoomHooksRecordings] Error: ' + e.response.data.message);
    }
  }
}
