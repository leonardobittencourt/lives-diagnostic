import AwsHelper from '../helpers/aws.helper';
import LogHelper from '../helpers/log.helper';
import { ILives } from './lives/interfaces/lives.interface';
import LivesRepository from './lives/lives.repository';
import { UPLOAD_PLATFORMS } from './recordings/enum/recordings.enum';
import { IRecordings } from './recordings/interfaces/recordings.interface';
import RecordingsRepository from './recordings/recordings.repository';
import VimeoService from './vimeo/vimeo.service';
import ZoomHooksRecordingsRepository from './zoom-hooks-recordings/zoom-hooks-recordings.repository';
import ZoomHooksParticipantsRepository from './zoom-hooks-participants/zoom-hooks-participants.repository';
import ZoomService from './zoom/zoom.service';
import * as fs from 'fs';
import LiveParticipantsRepository from './live-participants/lives.repository';
import ZoomHooksRecordingsService from './zoom-hooks-recordings/zoom-hooks-recordings.service';
import StatisticsService from './statistics/statistics.service';
import { ZoomHookParticipantsService } from './zoom-hooks-participants/zoom-hooks-participants.service';

export class AppService {
  private livesRepository;
  private liveParticipantsRepository;
  private zoomHooksRecordingsRepository;
  private zoomHooksParticipantsRepository;
  private recordingsRepository;
  private awsHelper;
  private vimeoService;
  private zoomService;
  private logHelper;
  private zoomHooksRecordingsServices;
  private statisticsService;
  private zoomHooksParticipantsService;

  constructor({
    livesRepository = new LivesRepository(),
    liveParticipantsRepository = new LiveParticipantsRepository(),
    zoomHooksRecordingsRepository = new ZoomHooksRecordingsRepository(),
    zoomHooksParticipantsRepository = new ZoomHooksParticipantsRepository(),
    recordingsRepository = new RecordingsRepository(),
    zoomHooksRecordingsServices = new ZoomHooksRecordingsService(),
    vimeoService = new VimeoService(),
    zoomService = new ZoomService(),
    statisticsService = new StatisticsService(),
    logHelper = new LogHelper(),
    awsHelper = new AwsHelper(),
    zoomHooksParticipantsService = new ZoomHookParticipantsService(),
  } = {}) {
    this.livesRepository = livesRepository;
    this.liveParticipantsRepository = liveParticipantsRepository;
    this.zoomHooksRecordingsRepository = zoomHooksRecordingsRepository;
    this.zoomHooksParticipantsRepository = zoomHooksParticipantsRepository;
    this.recordingsRepository = recordingsRepository;
    this.awsHelper = awsHelper;
    this.vimeoService = vimeoService;
    this.zoomService = zoomService;
    this.logHelper = logHelper;
    this.zoomHooksRecordingsServices = zoomHooksRecordingsServices;
    this.statisticsService = statisticsService;
    this.zoomHooksParticipantsService = zoomHooksParticipantsService;
  }

  async run() {
    const liveIds = this.readJsonFile();
    let index = 0;

    this.statisticsService.AddHeader([
      'Meeting ID',
      'Has Zoom Hooks',
      'Has zoom hooks participants',
      'Is on AWS S3',
      'Is on Vimeo',
      'Status Transfer',
    ]);

    for (const liveId of liveIds) {
      this.logHelper.log(`\n${liveId} - Start`);

      const results: any[] = [];
      const {
        live,
        liveParticipants,
        zoomRecordings,
        zoomParticipants,
        zoomHooksRecordings,
        zoomHooksParticipants,
        recordings,
      } = await this.checkServices(liveId);

      results.push({
        live,
        liveParticipants,
        zoomRecordings,
        zoomParticipants,
        zoomHooksRecordings,
        zoomHooksParticipants,
        recordings,
      });

      // when dont received the zoom hooks
      if (zoomHooksRecordings) {
        this.zoomService.generateSqlZoomHookRecordings([{ liveExternalId: live.live_external_id, ssoId: live.sso_id }]);
      }

      const awsS3Recordings = await this.getAWSS3Recordings(live, recordings);
      const vimeoRecordings = await this.getVimeoRecordings(live, recordings);

      const resultIndex = results.findIndex((r) => r.live.live_external_id === live.live_external_id);
      if (awsS3Recordings?.length > 0) results[resultIndex].recordings.push(awsS3Recordings);
      if (vimeoRecordings?.length > 0) results[resultIndex].recordings.push(vimeoRecordings);

      const getStatusTransfer = zoomHooksRecordings.find((f) => f.file_type == 'MP4');

      // show data o console
      this.statisticsService.AddRow(index, [
        live.live_external_id,
        (zoomHooksRecordings.length > 0).toString(),
        (zoomHooksParticipants.length > 0).toString(),
        (awsS3Recordings.length > 0).toString(),
        (vimeoRecordings.length > 0).toString(),
        getStatusTransfer ? getStatusTransfer.status_transfer : '---',
      ]);

      this.saveResultsToFile(live, results);

      this.logHelper.log(`${liveId} - Finished\n`);
      index++;
    }

    this.statisticsService.Show();
  }

  async checkServices(liveId: string) {
    //Get the live class in database
    const live = await this.livesRepository.findOneById(liveId);

    const liveParticipants = await this.liveParticipantsRepository.findByLiveId(liveId);

    //Get recordings on Zoom by liveExternalId
    const zoomRecordings = await this.zoomService.getRecordingByLiveExternalId(live.live_external_id);

    const zoomParticipants = await this.zoomService.getMeetingParticipantsReport(live.live_external_id);

    //Get all hooks of recordings by liveExternalId
    const zoomHooksRecordings = await this.zoomHooksRecordingsRepository.findOneByLiveExternalId(live.live_external_id);

    const zoomHooksParticipants = await this.zoomHooksParticipantsRepository.findOneByLiveExternalId(
      live.live_external_id,
    );

    const zoomHookParticipantsClassLeft = await this.zoomHooksParticipantsService.findInfoParticipandDontDataClassLeft(
      live,
    );

    //Get all recordings by liveExternalId
    const recordings = await this.recordingsRepository.findOneByLiveExternalId(live.live_external_id);

    return {
      live,
      liveParticipants: liveParticipants || [],
      zoomRecordings: zoomRecordings || [],
      zoomParticipants: zoomParticipants || [],
      zoomHooksRecordings: zoomHooksRecordings || [],
      zoomHooksParticipants: zoomHooksParticipants || [],
      recordings: recordings || [],
      zoomHookParticipantsClassLeft,
    };
  }

  async getAWSS3Recordings(live: ILives, recordings: IRecordings[]) {
    const results = [];

    const awsS3Recordings = recordings.filter((r) => r.uploaded_to === UPLOAD_PLATFORMS.AWS_S3);
    for (const recording of awsS3Recordings) {
      this.logHelper.log(`[AWS S3 Recordings] ${live.id} - Recordings - Start`);

      let { download_url: Key } = recording;
      Key = Key.substring(1);

      //Get the backup of the recoding on AWS S3
      const awsItem = await this.awsHelper.getObjectFromList(Key);
      const awsStorageClass = awsItem?.StorageClass;

      //Get recording from AWS S3
      const { url: awsUrl } = await this.awsHelper.getSignedUrlToGetObject(Key);

      results.push({ recording, awsStorageClass, awsUrl });

      this.logHelper.log(`[AWS S3 Recordings] ${live.id} - Recordings - Finished`);
    }

    return results;
  }

  async getVimeoRecordings(live: ILives, recordings: IRecordings[]) {
    let results = [];

    const vimeoRecordings = recordings.filter((r) => r.uploaded_to === UPLOAD_PLATFORMS.VIMEO);

    for (const recording of vimeoRecordings) {
      this.logHelper.log(`[Vimeo Recordings] ${live.id} - Recordings - Start`);

      const Key = recording.download_url;
      //Get the video on Vimeo
      let vimeoVideo;
      if (['vimeo'].includes(recording.uploaded_to)) {
        const vimeoResponse = await this.vimeoService.getVideoById(Key);
        vimeoVideo = vimeoResponse;
      }

      results.push({ recording, vimeoVideo });

      this.logHelper.log(`[Vimeo Recordings] ${live.id} - Recordings - Finished`);
    }

    return results;
  }

  readJsonFile(): string[] {
    let results: any[] = [];

    const { liveIds } = require(`${__dirname}/../data.json`);
    if (liveIds?.length <= 0) {
      this.logHelper.log('ERROR: You must input a list of liveId in the data.json file');
      return results;
    }

    results = liveIds;

    return results;
  }

  saveResultsToFile(live: ILives, results: any[], diferentDir: string = 'logs'): void {
    const dir = `${__dirname}/../results/${diferentDir}`;
    if (!fs.existsSync(dir)) fs.mkdirSync(dir, { recursive: true });

    fs.writeFileSync(`${dir}/${live.id}.json`, JSON.stringify(results, null, 2));
  }
}
