export interface IParticipant {
  id: string;
  user_id: string;
  name: string;
  user_email: string;
  join_time: Date;
  leave_time: Date;
  duration: number;
  attentiveness_score: string;
}

export interface IMeetingParticipants {
  page_count: number;
  page_size: number;
  total_records: number;
  next_page_token: string;
  participants: IParticipant[];
}
