import FileHelper from '../../helpers/file.helper';
import SqlHelper from '../../helpers/sql.helper';
import { ILives } from '../lives/interfaces/lives.interface';
import LivesRepository from '../lives/lives.repository';
import StatisticsService from '../statistics/statistics.service';
import ZoomService from '../zoom/zoom.service';
import { IZoomHooksParticipants } from './interfaces/zoom-hooks-participants.interface';
import ZoomHooksParticipantsRepository from './zoom-hooks-participants.repository';

export class ZoomHookParticipantsService {
  private livesRepository;
  private fileHelper;
  private statisticsService;
  private zoomHooksParticipantsRepository;
  private zoomService;
  private sqlHelper;

  constructor({
    livesRepository = new LivesRepository(),
    fileHelper = new FileHelper(),
    statisticsService = new StatisticsService(),
    zoomHooksParticipantsRepository = new ZoomHooksParticipantsRepository(),
    zoomService = new ZoomService(),
    sqlHelper = new SqlHelper(),
  } = {}) {
    this.livesRepository = livesRepository;
    this.fileHelper = fileHelper;
    this.statisticsService = statisticsService;
    this.zoomHooksParticipantsRepository = zoomHooksParticipantsRepository;
    this.zoomService = zoomService;
    this.sqlHelper = sqlHelper;
  }

  async findInfoParticipandDontDataClassLeft(live: ILives) {
    const infoParticipants = [];
    let zoomParticipants;

    this.statisticsService.AddHeader([
      'Meeting ID',
      'Name',
      'Hook - joined',
      'Hook - left',
      'Zoom - Joined',
      'Zoom - left',
      'Durations',
      'Email',
    ]);

    // get list user that no has info about class left
    const getParticipantsDontHasRegisterClassLeft: IZoomHooksParticipants[] = await this.zoomHooksParticipantsRepository.getParticipantsDontHasRegisterClassLeft(
      live.live_external_id,
    );

    if (getParticipantsDontHasRegisterClassLeft.length) {
      zoomParticipants = await this.zoomService.getMeetingParticipantsReport(live.live_external_id);

      for (let index in getParticipantsDontHasRegisterClassLeft) {
        const exists = zoomParticipants?.participants.find((zoomHook) =>
          new RegExp(zoomHook.name, 'g').test(getParticipantsDontHasRegisterClassLeft[index].participant_user_name),
        );

        if (exists) {
          infoParticipants.push({
            getParticipantsDontHasRegisterClassLeft: getParticipantsDontHasRegisterClassLeft[index],
            zoomParticipants: exists,
          });

          // set data on table terminal
          this.statisticsService.AddHeader([
            getParticipantsDontHasRegisterClassLeft[index].meeting_id,
            getParticipantsDontHasRegisterClassLeft[index].participant_user_name,
            getParticipantsDontHasRegisterClassLeft[index].meeting_start_time,
            '---',
            exists?.join_time.toString() || '',
            exists?.leave_time.toString() || '',
            exists?.duration.toString() || '',
            exists?.user_email || '',
          ]);
        }
      }
      // create sql to save database
      const query = this.sqlHelper.createQueryZoomHooksParticipants(infoParticipants);
      // save sql
      this.fileHelper.saveLog(query, `${live.live_external_id}.sql`, '../sql/ZoomHooksParticipants');
    }

    // save file log
    if (infoParticipants?.length) {
      this.fileHelper.saveResultsToFile(live, infoParticipants, 'participantsClassLeft');
    }

    return infoParticipants;
  }
}
