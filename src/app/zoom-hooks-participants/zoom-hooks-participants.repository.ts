import Knex from 'knex';
import LogHelper from '../../helpers/log.helper';
import { ZoomHooksConnection } from '../../database/connection';
import { IZoomHooksParticipants } from './interfaces/zoom-hooks-participants.interface';
import { EventNameEnum } from './enum/event-name.enum';

export default class ZoomHooksParticipantsRepository {
  private readonly tableName = 'zoom_hook_participants';
  private readonly logHelper;
  private readonly connection: Knex;

  constructor({ logHelper = new LogHelper(), connection = ZoomHooksConnection } = {}) {
    this.logHelper = logHelper;
    this.connection = connection;
  }

  async findOneByLiveExternalId(liveExternalId: string): Promise<IZoomHooksParticipants[]> {
    return await this.connection
      .table<IZoomHooksParticipants>(this.tableName)
      .select('*')
      .where('meeting_id', '=', liveExternalId)
      .orderBy('created_at', 'desc');
  }

  async getParticipantsDontHasRegisterClassLeft(liveExternalId: string): Promise<IZoomHooksParticipants[]> {
    const participants = await this.findOneByLiveExternalId(liveExternalId);
    let hashParticipants = participants.filter((f) => f.event_name === EventNameEnum.JOINED);

    for (let index in participants) {
      // remove our add on hash participants, to descorver iof exists student that no exit class(error hook zoom dont sent)
      if (participants[index].event_name === EventNameEnum.LEFT) {
        hashParticipants = hashParticipants.filter((participant) => {
          return participant.participant_user_name !== participants[index].participant_user_name;
        });
      }
    }

    return hashParticipants;
  }
}
