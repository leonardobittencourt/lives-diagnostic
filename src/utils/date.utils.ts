import * as moment from 'moment';

export const formatDate = (date: Date): string => {
  return moment.utc(date).format('YYYY-MM-DD HH:mm:ss');
};
