if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config({ path: __dirname + '/../../.env' });
}

module.exports = {
  development: {
    client: 'mysql',
    connection: {
      host: process.env.KNEX_HOST,
      port: Number(process.env.KNEX_PORT),
      user: process.env.KNEX_USER,
      password: process.env.KNEX_PASSWORD,
      database: process.env.KNEX_ZOOM_HOOKS_DATABASE,
    },
  },
  production: {
    client: 'mysql',
    connection: {
      host: process.env.KNEX_HOST,
      port: Number(process.env.KNEX_PORT),
      user: process.env.KNEX_USER,
      password: process.env.KNEX_PASSWORD,
      database: process.env.KNEX_ZOOM_HOOKS_DATABASE,
    },
  },
};
