import axios, { AxiosRequestConfig } from 'axios';
import { differenceInSeconds } from 'date-fns';
import LogHelper from './log.helper';

export default class HttpHelper {
  private logHelper;
  private checkApiRateLimit: boolean = false;
  private limitRequestsPerSecond: number = 0;
  private lastRequest: Date | null = null;
  private requestCount = 0;
  private timeToRest = 0;

  constructor({
    logHelper = new LogHelper(),
    checkApiRateLimit = false,
    limitRequestsPerSecond = 0,
    timeToRest = 1000,
  } = {}) {
    this.logHelper = logHelper;
    this.checkApiRateLimit = checkApiRateLimit;
    this.limitRequestsPerSecond = limitRequestsPerSecond;
    this.timeToRest = timeToRest;
  }

  async get<T>(url: string, config?: AxiosRequestConfig) {
    await this.checkRequestLimit();
    this.requestCounter();
    return await axios.get<T>(url, config);
  }

  async post<T>(url: string, data: any, config?: AxiosRequestConfig) {
    await this.checkRequestLimit();
    this.requestCounter();
    return await axios.post<T>(url, data, config);
  }

  async delete(url: string, config: AxiosRequestConfig) {
    await this.checkRequestLimit();
    this.requestCounter();
    return await axios.delete(url, config);
  }

  private async checkRequestLimit() {
    if (!this.checkApiRateLimit) {
      return null;
    }

    const diffSeconds = differenceInSeconds(new Date(), this.lastRequest || new Date());

    if (diffSeconds === 0 && this.requestCount === this.limitRequestsPerSecond) {
      this.logHelper.log('[API-RATE-LIMIT] TIME TO REST');
      await this.sleep(this.timeToRest);

      this.lastRequest = null;
      this.requestCount = 0;
      this.logHelper.log('[API-RATE-LIMIT] TIME TO WORK');
    }

    if (diffSeconds > 1) {
      this.lastRequest = null;
      this.requestCount = 0;
    }
  }

  private sleep(ms: number) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(true);
      }, ms);
    });
  }

  private requestCounter() {
    if (!this.checkApiRateLimit) {
      return null;
    }

    this.lastRequest = new Date();
    this.requestCount += 1;
  }
}
