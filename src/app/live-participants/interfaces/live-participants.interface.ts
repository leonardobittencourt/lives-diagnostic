export interface ILiveParticipants {
  id: string;
  sso_id: string;
  registrant_id: string;
  personal_join_url: string;
  live_external_id: string;
  platform: string;
  live_id: string;
  user_id: string;
  created_at: string;
  updated_at: string;
  deleted_at: string;
  role: string;
  sso_name: string;
}
